#include <stdio.h>
#include "libhello_world.h"
#include "libgoodbye_world.h"

int main()
{
	char hello[14];
	char goodbye[16];
	get_hello(hello, sizeof(hello));
	printf("%s\n", hello);
	get_goodbye(goodbye, sizeof(goodbye));
	printf("%s\n", goodbye);
	return 0;
}
